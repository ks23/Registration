﻿using System;
using BLL.Services.PersonService;
using BLL.Services.PersonService.Interfaces;
using BLL.Services.UserService;
using BLL.Services.UserService.Interfaces;
using DAL.Contexts;
using DAL.Entities;
using DAL.Entities.Identity;
using DAL.Repositories;
using DAL.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MVC.ApplicationServices.AccountApplicationService;
using MVC.ApplicationServices.Identity;
using MVC.ApplicationServices.UserApplicationService;
using MVC.ApplicationServices.UserApplicationService.Interfaces;
using MVC.Commands;
using MVC.ApplicationServices.EmailSender;
using MVC.DataSeeders;
using MVC.DataSeeders.Interfaces;

namespace MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<RegistrationDbContext>();

            services.AddIdentity<UserEntity, RoleEntity>(options =>
                {
                    // User settings
                    options.User.RequireUniqueEmail = true;

                    // Password settings
                    options.Password.RequireDigit = true;
                    options.Password.RequiredLength = 6;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireLowercase = false;

                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                    options.Lockout.MaxFailedAccessAttempts = 10;
                })
                .AddUserStore<ApplicationUserStore>()
                .AddUserManager<ApplicationUserManager>()
                .AddRoleStore<ApplicationRoleStore>()
                .AddRoleManager<ApplicationRoleManager>()
                .AddSignInManager<ApplicationSignInManager>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<RegistrationDbContext>();

            services.AddScoped<UserStore<UserEntity, RoleEntity, RegistrationDbContext, int, UserClaimEntity, UserRoleEntity, UserLoginEntity, UserTokenEntity, RoleClaimEntity>, ApplicationUserStore>();
            services.AddScoped<UserManager<UserEntity>, ApplicationUserManager>();
            services.AddScoped<RoleManager<RoleEntity>, ApplicationRoleManager>();
            services.AddScoped<SignInManager<UserEntity>, ApplicationSignInManager>();
            services.AddScoped<RoleStore<RoleEntity, RegistrationDbContext, int, UserRoleEntity, RoleClaimEntity>, ApplicationRoleStore>();
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<IApplicationDataSeeder, ApplicationDataSeeder>();

            // Pick Auto
            // Repositories
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            // BLL Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPersonService, PersonService>();

            // Application Services
            services.AddScoped<IUserApplicationService, UserApplicationService>();
            services.AddScoped<IAccountApplicationService, AccountApplicationService>();

            services.AddScoped<RegistrationSaveCommand>();
            services.AddScoped<RegistrationDbContext>();

            services.AddScoped<RegistrationDbContext>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env, 
            IApplicationDataSeeder applicationDataSeeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                applicationDataSeeder.SeedData();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
