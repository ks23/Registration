using System.Threading.Tasks;
using DAL.Contexts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MVC.ApplicationServices.AccountApplicationService.Models;
using MVC.Commands;
using MVC.Extensions;

namespace MVC.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : ExtendedPageModel
    {
        private readonly RegistrationSaveCommand _registrationSaveCommand;
        private readonly RegistrationDbContext _dbContext;

        public RegisterModel(
            RegistrationSaveCommand registrationSaveCommand,
            RegistrationDbContext dbContext)
        {
            this._registrationSaveCommand = registrationSaveCommand;
            this._dbContext = dbContext;
        }

        [BindProperty]
        public RegisterViewModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            return await CommandAsync(
                _registrationSaveCommand,
                _dbContext,
                cmd => cmd.ViewModel = Input,
                cmd => LocalRedirect(returnUrl = returnUrl ?? Url.Content("~/")),
                Page);
        }
    }
}
