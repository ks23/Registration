using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Logic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MVC.ApplicationServices.UserApplicationService.Interfaces;
using MVC.ApplicationServices.UserApplicationService.Models;

namespace MVC.Areas.Administration.Pages.User
{
    public class RegisteredUsersModel : PageModel
    {
        private readonly IUserApplicationService _userApplicationService;

        public RegisteredUsersModel(
            IUserApplicationService userApplicationService)
        {
            _userApplicationService = userApplicationService;
        }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string NameFilter { get; set; }
        public string EmailFilter { get; set; }
        public string CurrentSort { get; set; }
        public PaginatedList<UserListItemViewModel> RegisteredUsers { get; set; }
        public UserFilterViewModel Filter { get; set; }

        public async Task OnGetAsync(string sortOrder, string nameFilter, string emailFilter, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            DateSort = sortOrder == "Date" ? "date_desc" : "Date";
            NameFilter = nameFilter;
            EmailFilter = emailFilter;

            var users = _userApplicationService.GetUsers(new UserFilterViewModel {Email = emailFilter, Name = nameFilter });

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.FullName);
                    break;
                case "Date":
                    users = users.OrderBy(s => s.DateOfBirth);
                    break;
                case "date_desc":
                    users = users.OrderByDescending(s => s.DateOfBirth);
                    break;
                default:
                    users = users.OrderBy(s => s.FullName);
                    break;
            }

            var pageSize = 10;
            RegisteredUsers = await PaginatedList<UserListItemViewModel>.CreateAsync(
                users.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}