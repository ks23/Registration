﻿using System;
using BLL.Logic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace MVC.Extensions
{
    public class ExtendedPageModel : PageModel
    {
        protected async System.Threading.Tasks.Task<ActionResult> CommandAsync<TCommand>(
            TCommand command,
            DbContext dbContext,
            Action<TCommand> initCommand, 
            Func<TCommand, ActionResult> success, 
            Func<ActionResult> failure) where TCommand : ICommand
        {
            initCommand(command);

            if (!ModelState.IsValid)
                return failure();

            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    await command.Execute();
                    transaction.Commit();
                    return success(command);

                }
                catch (RulesException ex)
                {
                    transaction.Rollback();

                    foreach (var error in ex.Errors)
                    {
                        ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                    }

                    return failure();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return failure();
                }
            }
        }
    }
}
