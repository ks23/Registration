﻿using System.Threading.Tasks;

namespace MVC.DataSeeders.Interfaces
{
    public interface IApplicationDataSeeder
    {
        void SeedData();
    }
}
