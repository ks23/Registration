﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Logic;
using DAL.Contexts;
using MVC.ApplicationServices.AccountApplicationService.Models;
using MVC.ApplicationServices.UserApplicationService.Interfaces;
using MVC.ApplicationServices.UserApplicationService.Models;
using MVC.Commands;
using MVC.DataSeeders.Interfaces;
using Tools.Generators;

namespace MVC.DataSeeders
{
    public class ApplicationDataSeeder : IApplicationDataSeeder
    {
        private readonly IUserApplicationService _userApplicationService;
        private readonly RegistrationSaveCommand _registrationSaveCommand;

        public ApplicationDataSeeder(
            IUserApplicationService userApplicationService,
            RegistrationSaveCommand registrationSaveCommand)
        {
            _userApplicationService = userApplicationService;
            _registrationSaveCommand = registrationSaveCommand;
        }


        public async void SeedData()
        {
            SeedUsers();
        }

        #region Seed Users
        private async void SeedUsers()
        {
            //var existingUsers = _userApplicationService.GetUsers(new UserFilterViewModel());

            //if (existingUsers.Any())
            //{
            //    return;
            //}

            await CommandAsync(
                _registrationSaveCommand,
                cmd =>
                {
                    cmd.ClearErrors();
                    cmd.ViewModel =
                        new RegisterViewModel
                        {
                            FirstName = "John",
                            LastName = "Baron",
                            AdditionalInfo = "Additional Info",
                            DateOfBirth = DateTime.Now.AddYears(-30),
                            Email = "admin@admin.com",
                            Password = "Admin1!",
                            ConfirmPassword = "Admin1!"
                        };
                });

            for (var i = 0; i < 50; i++)
            {
                await CommandAsync(_registrationSaveCommand, cmd =>
                {
                    cmd.ClearErrors();
                    cmd.ViewModel = GetRegisterViewModel();
                });
            }
        }

        private RegisterViewModel GetRegisterViewModel()
        {
            return new RegisterViewModel
            {
                FirstName = StringGenerator.Generate(10),
                LastName = StringGenerator.Generate(10),
                AdditionalInfo = StringGenerator.GenerateWithNumbers(20),
                DateOfBirth = DateTime.Now.AddYears(-30),
                Email = StringGenerator.GenerateWithNumbers(30) + "@gmail.com",
                Password = "Admin1!",
                ConfirmPassword = "Admin1!"
            };
        }
        #endregion

        #region Command
        protected async Task CommandAsync<TCommand>(
            TCommand command,
            Action<TCommand> initCommand) where TCommand : ICommand
        {
            initCommand(command);
            var context = new RegistrationDbContext();

            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    await command.Execute();
                    transaction.Commit();
                }
                catch (RulesException ex)
                {
                    transaction.Rollback();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }
        }
        #endregion

    }
}
