﻿using System.Threading.Tasks;
using BLL.Infrastructure;
using BLL.Logic;
using BLL.Services.UserService.Interfaces;
using MVC.ApplicationServices.AccountApplicationService;
using MVC.ApplicationServices.AccountApplicationService.Models;

namespace MVC.Commands
{
    public class RegistrationSaveCommand : CommandBase
    {
        private readonly IAccountApplicationService _accountApplicationService;
        private readonly IUserService _userService;

        public RegistrationSaveCommand(
            IAccountApplicationService accountApplicationService,
            IUserService userService)
        {
            _accountApplicationService = accountApplicationService;
            _userService = userService;
        }

        public RegisterViewModel ViewModel { get; set; }

        protected override async Task ExecuteCommand()
        {
            await _accountApplicationService.RegisterUserAsync(ViewModel);
        }

        protected override async Task<bool> Validate()
        {
            if (_userService.CheckIfEmailExists(ViewModel.Email))
            {
                AddError("Email", ErrorConstants.SuchAccountAlreadyExists + $": {ViewModel.Email}");
            }

            if (ViewModel.Password != ViewModel.ConfirmPassword)
            {
                AddError("ConfirmPassword", ErrorConstants.PasswordsDoNotMatch);
            }

            return await base.Validate();
        }
    }
}
