﻿using System.Security.Claims;
using DAL;
using DAL.Contexts;
using DAL.Entities;
using DAL.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MVC.ApplicationServices.Identity
{
    public class ApplicationUserStore : UserStore<UserEntity, RoleEntity, RegistrationDbContext, int, UserClaimEntity, UserRoleEntity, UserLoginEntity, UserTokenEntity, RoleClaimEntity>
    {
        public ApplicationUserStore(RegistrationDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        protected override UserRoleEntity CreateUserRole(UserEntity user, RoleEntity role)
        {
            return new UserRoleEntity { RoleId = role.Id, UserId = user.Id };
        }

        protected override UserClaimEntity CreateUserClaim(UserEntity user, Claim claim)
        {
            return new UserClaimEntity { ClaimValue = claim.Value, ClaimType = claim.Type, UserId = user.Id };
        }

        protected override UserLoginEntity CreateUserLogin(UserEntity user, UserLoginInfo login)
        {
            return new UserLoginEntity
            {
                LoginProvider = login.LoginProvider,
                ProviderDisplayName = login.ProviderDisplayName,
                ProviderKey = login.ProviderKey,
                UserId = user.Id
            };
        }

        protected override UserTokenEntity CreateUserToken(UserEntity user, string loginProvider, string name, string value)
        {
            return new UserTokenEntity
            {
                LoginProvider = loginProvider,
                Name = name,
                UserId = user.Id,
                Value = value
            };
        }
    }
}
