﻿using System.Security.Claims;
using DAL;
using DAL.Contexts;
using DAL.Entities;
using DAL.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MVC.ApplicationServices.Identity
{
    public class ApplicationRoleStore : RoleStore<RoleEntity, RegistrationDbContext, int, UserRoleEntity, RoleClaimEntity>
    {
        public ApplicationRoleStore(
            RegistrationDbContext context,
            IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }

        protected override RoleClaimEntity CreateRoleClaim(RoleEntity role, Claim claim)
        {
            return new RoleClaimEntity
            {
                RoleId = role.Id,
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            };
        }
    }
}
