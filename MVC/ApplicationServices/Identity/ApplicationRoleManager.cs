﻿using System.Collections.Generic;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace MVC.ApplicationServices.Identity
{
    public class ApplicationRoleManager : RoleManager<RoleEntity>
    {
        public ApplicationRoleManager(
            IRoleStore<RoleEntity> store,
            IEnumerable<IRoleValidator<RoleEntity>> roleValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            ILogger<RoleManager<RoleEntity>> logger) 
        : base(store, roleValidators, keyNormalizer, errors,logger)
        {
        }
    }
}
