﻿using System.Threading.Tasks;
using DAL.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MVC.ApplicationServices.Identity
{
    public class ApplicationSignInManager : SignInManager<UserEntity>
    {
        public ApplicationSignInManager(UserManager<UserEntity> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<UserEntity> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<UserEntity>> logger, IAuthenticationSchemeProvider scheme) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, scheme)
        {
        }

        public override Task<SignInResult> PasswordSignInAsync(UserEntity user, string password, bool rememberMe, bool lockoutOnFailure)
        {
            if (!user.IsValid)
            {
                return Task.FromResult(SignInResult.LockedOut);
            }

            return base.PasswordSignInAsync(user, password, rememberMe, lockoutOnFailure);
        }
    }
}
