﻿using System.Linq;
using MVC.ApplicationServices.UserApplicationService.Models;

namespace MVC.ApplicationServices.UserApplicationService.Interfaces
{
    public interface IUserApplicationService
    {
        IQueryable<UserListItemViewModel> GetUsers(UserFilterViewModel filter);
    }
}
