﻿using System.Linq;
using BLL.Services.UserService.Interfaces;
using MVC.ApplicationServices.UserApplicationService.Interfaces;
using MVC.ApplicationServices.UserApplicationService.Models;

namespace MVC.ApplicationServices.UserApplicationService
{
    public class UserApplicationService : IUserApplicationService
    {
        private readonly IUserService _userService;


        public UserApplicationService(IUserService userService)
        {
            this._userService = userService;
        }

        public IQueryable<UserListItemViewModel> GetUsers(UserFilterViewModel filter)
        {
            return _userService.GetUsers<UserListItemViewModel>(filter);
        }
    }
}
