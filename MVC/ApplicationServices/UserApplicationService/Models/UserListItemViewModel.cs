﻿using System.ComponentModel.DataAnnotations;
using BLL.Services.UserService.Interfaces;

namespace MVC.ApplicationServices.UserApplicationService.Models
{
    public class UserListItemViewModel : IUserListItemModel
    {
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string FullName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Date Of Birth")]
        public string DateOfBirth { get; set; }
    }
}
