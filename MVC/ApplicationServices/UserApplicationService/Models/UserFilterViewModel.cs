﻿using BLL.Services.UserService.Interfaces;

namespace MVC.ApplicationServices.UserApplicationService.Models
{
    public class UserFilterViewModel : IUserFilterModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
