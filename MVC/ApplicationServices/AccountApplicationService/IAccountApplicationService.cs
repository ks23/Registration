﻿using System.Linq;
using System.Threading.Tasks;
using BLL.Logic;
using BLL.Services.PersonService.Interfaces;
using BLL.Services.UserService.Interfaces;
using MVC.ApplicationServices.AccountApplicationService.Models;
using MVC.ApplicationServices.Identity;

namespace MVC.ApplicationServices.AccountApplicationService
{
    public interface IAccountApplicationService
    {
        Task RegisterUserAsync(RegisterViewModel viewModel);
    }

    public class AccountApplicationService : IAccountApplicationService
    {
        private readonly IUserService _userService;
        private readonly IPersonService _personService;
        private readonly ApplicationUserManager _userManager;

        public AccountApplicationService(
            IUserService userService,
            ApplicationUserManager userManager,
            IPersonService personService)
        {
            _userService = userService;
            _userManager = userManager;
            _personService = personService;
        }

        public async Task RegisterUserAsync(RegisterViewModel viewModel)
        {
            var person = _personService.SaveNewPerson(viewModel);
            var user = _userService.GetNewUserEntity(viewModel.Email, person.Id);

            var result = await _userManager.CreateAsync(user, viewModel.Password);
            if (!result.Succeeded)
            {
                throw new RulesException(result.Errors.Select(s => new ErrorInfo(string.Empty, s.Description)));
            }
        }
    }

}
