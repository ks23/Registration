﻿using BLL.Services.PersonService.Interfaces;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace BLL.Services.PersonService
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;

        public PersonService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public PersonEntity SaveNewPerson(IPersonInfoModel model)
        {
            var personEntity = new PersonEntity
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                AdditionalInfo = model.AdditionalInfo,
                DateOfBirth = model.DateOfBirth
            };

            _personRepository.Add(personEntity);
            _personRepository.SaveChanges();

            return personEntity;
        }
    }
}
