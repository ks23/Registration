﻿using System;
using BLL.Services.PersonService.Interfaces;

namespace BLL.Services.PersonService.Models
{
    public class PersonInfoModel : IPersonInfoModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string AdditionalInfo { get; set; }
    }
}
