﻿using DAL.Entities;

namespace BLL.Services.PersonService.Interfaces
{
    public interface IPersonService
    {
        PersonEntity SaveNewPerson(IPersonInfoModel model);
    }
}
