﻿using System;

namespace BLL.Services.PersonService.Interfaces
{
    public interface IPersonInfoModel
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        DateTime DateOfBirth { get; set; }

        string AdditionalInfo { get; set; }
    }
}
