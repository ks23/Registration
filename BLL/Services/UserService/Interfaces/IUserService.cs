﻿using System.Linq;
using DAL.Entities;

namespace BLL.Services.UserService.Interfaces
{
    public interface IUserService
    {
        IQueryable<T> GetUsers<T>(IUserFilterModel filter) where T : IUserListItemModel, new();
        bool CheckIfEmailExists(string email);
        UserEntity GetNewUserEntity(string email, int personId, bool isValid = true);
    }
}
