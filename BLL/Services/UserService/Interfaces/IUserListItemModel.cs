﻿namespace BLL.Services.UserService.Interfaces
{
    public interface IUserListItemModel
    {
        int Id { get; set; }
        string FullName { get; set; }
        string Email { get; set; }
        string DateOfBirth { get; set; }
    }
}
