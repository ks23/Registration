﻿namespace BLL.Services.UserService.Interfaces
{
    public interface IUserFilterModel
    {
        string Email { get; set; }
        string Name { get; set; }
    }
}
