﻿using System.Linq;
using BLL.Services.UserService.Interfaces;
using DAL.Entities;
using DAL.Repositories.Interfaces;
using Tools.Formatters;

namespace BLL.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPersonRepository _personRepository;

        public UserService(
            IUserRepository userRepository,
            IPersonRepository personRepository)
        {
            _userRepository = userRepository;
            _personRepository = personRepository;
        }

        public IQueryable<T> GetUsers<T>(IUserFilterModel filter) where T : IUserListItemModel, new()
        {
            var data =
                from user in _userRepository.GetAll()
                join person in _personRepository.GetAll()
                    on user.PersonId equals person.Id
                where 
                    (filter.Name == null || person.FirstName.Contains(filter.Name) || person.LastName.Contains(filter.Name))
                    && (filter.Email == null || user.Email.Contains(filter.Email))
                select new T
                {
                    Id = user.Id,
                    FullName = PersonFormatter.FormatFullName(person.FirstName, person.LastName),
                    DateOfBirth = DateTimeFormatter.GetShortDate(person.DateOfBirth),
                    Email = user.Email
                };

            return data;
        }

        public bool CheckIfEmailExists(string email)
        {
            return _userRepository.GetAll().Any(x => x.Email == email);
        }

        public UserEntity GetNewUserEntity(string email, int personId, bool isValid = true)
        {
            return new UserEntity { UserName = email, Email = email, PersonId = personId, IsValid = isValid };
        }
    }
}
