﻿using BLL.Services.UserService.Interfaces;

namespace BLL.Services.UserService.Models
{
    public class UserFilterModel : IUserFilterModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
