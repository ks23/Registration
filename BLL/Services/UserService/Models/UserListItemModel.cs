﻿using BLL.Services.UserService.Interfaces;

namespace BLL.Services.UserService.Models
{
    public class UserListItemModel : IUserListItemModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
    }
}
