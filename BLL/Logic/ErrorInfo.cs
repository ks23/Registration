﻿namespace BLL.Logic
{
    public class ErrorInfo
    {
        public string PropertyName { get; private set; }
        public string ErrorMessage { get; private set; }

        public ErrorInfo(string propertyName, string errorMessage)
        {
            PropertyName = propertyName;
            ErrorMessage = errorMessage;
        }

        protected bool Equals(ErrorInfo other)
        {
            return string.Equals(PropertyName, other.PropertyName) && string.Equals(ErrorMessage, other.ErrorMessage);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(ErrorInfo)) return false;
            return Equals((ErrorInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((PropertyName != null ? PropertyName.GetHashCode() : 0) * 397) ^ (ErrorMessage != null ? ErrorMessage.GetHashCode() : 0);
            }
        }

        public static bool operator ==(ErrorInfo left, ErrorInfo right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ErrorInfo left, ErrorInfo right)
        {
            return !Equals(left, right);
        }
    }
}
