﻿using System.Threading.Tasks;

namespace BLL.Logic
{
    public interface ICommand
    {
        Task Execute();
    }
}
