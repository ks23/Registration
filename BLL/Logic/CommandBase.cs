﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BLL.Infrastructure;
using DAL.Contexts;
using Microsoft.EntityFrameworkCore;
using Tools;

namespace BLL.Logic
{
    public abstract class CommandBase : ICommand
    {
        protected RegistrationDbContext DbContext;
        protected List<ErrorInfo> Errors { get; set; }
        protected long DeadlockExecuteTries { get; set; }
        protected long PendingRequestsOnTransactionTries { get; set; }

        protected CommandBase()
        {
            Errors = new List<ErrorInfo>();
            DeadlockExecuteTries = 0;
            PendingRequestsOnTransactionTries = 0;
            DbContext = new RegistrationDbContext();
        }

        public void AddError(string errorMessage)
        {
            AddError(string.Empty, errorMessage);
        }

        public void AddError(string propertyName, string errorMessage)
        {
            Errors.Add(new ErrorInfo(propertyName, errorMessage));
        }

        public void AddRequiredFieldError(string propertyName, string fieldTitle)
        {
            var errorMessage = $"Field {fieldTitle} is required.";
            Errors.Add(new ErrorInfo(propertyName, errorMessage));
        }

        public void AddError(ErrorInfo errorInfo)
        {
            Errors.Add(errorInfo);
        }

        public void AddErrors(ICollection<ErrorInfo> errorInfos)
        {
            if (errorInfos != null && errorInfos.Count > 0)
            {
                Errors.AddRange(errorInfos);
            }
        }

        public void AddErrors(IEnumerable<ErrorInfo> errorInfos)
        {
            if (errorInfos == null) return;

            var enumerable = errorInfos.ToList();
            if (enumerable.Any())
            {
                Errors.AddRange(enumerable);
            }
        }

        public void ClearErrors()
        {
            Errors = new List<ErrorInfo>();
        }

        public virtual async Task Execute()
        {
            using (var transaction = DbContext.Database.BeginTransaction())
            {
                if (!(await Validate()))
                {
                    throw new RulesException(Errors);
                }

                try
                {
                    await ExecuteCommand();
                    transaction.Commit();
                }
                catch (RulesException e)
                {
                    throw new RulesException(e.Errors);
                }
                catch (Exception e)
                {
                    if (IsDeadlock(e) && DeadlockExecuteTries < 3)
                    {
                        DeadlockExecuteTries++;
                        await Execute();
                        return;
                    }

                    if (PendingRequestsOnTransaction(e) && PendingRequestsOnTransactionTries < 3)
                    {
                        PendingRequestsOnTransactionTries++;
                        await Execute();
                        return;
                    }

                    VerifyDbSpecificExceptionInfo(e);
                    throw;
                }
                finally
                {
                    OnExecuted();
                }
            }
        }

        protected virtual void VerifyDbSpecificExceptionInfo(Exception e)
        {
            var ex = e as DbUpdateException;
            if (ex == null && e.InnerException != null)
            {
                ex = e.InnerException as DbUpdateException;
            }

            if (ex == null) return;

            // Parse SQL Error Code Here
            var errorCode = ex.Message;

            if (errorCode == MsSqlServerSystemMessages.ViolationOfConstraintCannotInsertDuplicateKey
                || errorCode == MsSqlServerSystemMessages.CannotInsertDuplicateKeyRowWithUniqueIndex)
            {
                AddError(ErrorConstants.SuchEntryAlreadyExists);
                throw new RulesException(Errors);
            }
        }

        protected virtual bool IsDeadlock(Exception e)
        {
            return (e is SqlException sqlEx && sqlEx.Number.ToString() == MsSqlServerSystemMessages.TransactionWasDeadlocked)
                   || (e.InnerException != null && IsDeadlock(e.InnerException));
        }

        protected virtual bool PendingRequestsOnTransaction(Exception e)
        {
            return (e is SqlException sqlEx && sqlEx.Number.ToString() == MsSqlServerSystemMessages.PendingRequestsWorkingOnTransaction)
                   || (e.InnerException != null && PendingRequestsOnTransaction(e.InnerException));
        }

        protected abstract Task ExecuteCommand();

        public virtual void OnExecuted()
        {
        }

        protected virtual async Task<bool> Validate()
        {
            return Errors.Count == 0;
        }
    }
}
