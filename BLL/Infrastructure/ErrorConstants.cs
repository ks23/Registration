﻿namespace BLL.Infrastructure
{
    public class ErrorConstants
    {
        public const string SuchEntryAlreadyExists = "Such an entry already exists.";
        public const string SuchEntryAlreadyExistsAsInvalid = "Such an entry already exists as invalid.";
        public const string SuchAccountAlreadyExists = "Such email account name already exists.";
        public const string PasswordsDoNotMatch = "Passwords do not match.";
        public const string FieldIsRequired = "Field is required";
        public const string EndDateCannotBeEarlerThanStartDate = "End date cannot be earler than start date.";
        public const string SelectAtLeastOneRecord = "Select at least one record";
        public const string SessionHasExpired = "Session has expired. Please log-in again";
        public const string AddAtLeastOneRecord = "Add at least one record";
        public const string ErrorNotification = "Sorry, an error occurred while processing your request. Please contact the administrator.";
        public const string FileSizeLimit = "The files you are trying to send exceeds the 25 MB attachment limit";
        public const string Timeout = "Timeout";
        public const string SpecialCharacters = "Special characters are not allowed";
        public const string YouAreNotLoggedIn = "You are not logged in. Please log in to see this page.";
        public const string NoSender = "Please select sender.";
        public const string SuchItemNoLongerExists = "Such Item No Longer Exists!";
        public const string SqlConnectivityError = "Unable to connect to SQL server";
    }
}
