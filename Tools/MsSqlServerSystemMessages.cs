﻿namespace Tools
{
    public static class MsSqlServerSystemMessages
    {
        public const string CannotInsertDuplicateKeyRowWithUniqueIndex = "2601";
        public const string ViolationOfConstraintCannotInsertDuplicateKey = "2627";
        public const string TransactionWasDeadlocked = "1205";
        public const string TimeoutExpired = "-2";
        public const string PendingRequestsWorkingOnTransaction = "3981";
    }
}
