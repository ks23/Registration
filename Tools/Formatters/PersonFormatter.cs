﻿namespace Tools.Formatters
{
    public class PersonFormatter
    {
        public static string FormatFullName(string firstName, string lastName)
        {
            return $"{firstName} {lastName}";
        }
    }
}
