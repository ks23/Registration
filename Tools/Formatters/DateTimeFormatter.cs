﻿using System;

namespace Tools.Formatters
{
    public static class DateTimeFormatter
    {
        public static string GetShortDate(DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.ToShortDateString() : null;
        }
    }
}
