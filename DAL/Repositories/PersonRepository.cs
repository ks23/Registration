﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories.Base;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class PersonRepository : Repository<PersonEntity, RegistrationDbContext>, IPersonRepository
    {
        public PersonRepository(RegistrationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
