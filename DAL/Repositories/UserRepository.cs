﻿using DAL.Contexts;
using DAL.Entities;
using DAL.Repositories.Base;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories
{
    public class UserRepository : Repository<UserEntity, RegistrationDbContext>, IUserRepository
    {
        public UserRepository(RegistrationDbContext dbContext) : base(dbContext)
        {
        }
    }
}
