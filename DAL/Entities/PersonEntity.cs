﻿using System;
using System.Collections.Generic;
using DAL.Entities.Base;

namespace DAL.Entities
{
    public class PersonEntity : BaseAuditableEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AdditionalInfo { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public ICollection<UserEntity> Users { get; set; }
    }
}
