﻿using System;
using System.ComponentModel.DataAnnotations;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace DAL.Entities
{
    public class UserEntity : IdentityUser<int>, IAuditableEntity
    {
        public bool IsValid { get; set; }

        public int PersonId { get; set; }

        public PersonEntity Person { get; set; }

        [MaxLength(256)]
        public string CreatedBy { get; set; }

        [MaxLength(256)]
        public string UpdatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
