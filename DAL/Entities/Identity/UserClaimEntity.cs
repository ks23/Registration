﻿using System;
using System.ComponentModel.DataAnnotations;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace DAL.Entities
{
    public class UserClaimEntity : IdentityUserClaim<int>, IAuditableEntity
    {
        [MaxLength(256)]
        public string CreatedBy { get; set; }

        [MaxLength(256)]
        public string UpdatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
