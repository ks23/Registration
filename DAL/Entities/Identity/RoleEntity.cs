﻿using System;
using System.ComponentModel.DataAnnotations;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace DAL.Entities
{
    public class RoleEntity : IdentityRole<int>, IAuditableEntity
    {
        public RoleEntity()
        {
        }

        public RoleEntity(string roleName) : base(roleName)
        {
        }

        [MaxLength(256)]
        public string CreatedBy { get; set; }

        [MaxLength(256)]
        public string UpdatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
