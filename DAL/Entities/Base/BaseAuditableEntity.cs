﻿using System;
using System.ComponentModel.DataAnnotations;
using DAL.Interfaces;

namespace DAL.Entities.Base
{
    public class BaseAuditableEntity : BaseEntity, IAuditableEntity
    {
        [MaxLength(256)]
        public string CreatedBy { get; set; }

        [MaxLength(256)]
        public string UpdatedBy { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
