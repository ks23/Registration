﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Entities.Identity;
using DAL.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.Contexts
{
    public class RegistrationDbContext : IdentityDbContext<UserEntity, RoleEntity, int, UserClaimEntity, UserRoleEntity, UserLoginEntity, RoleClaimEntity, UserTokenEntity>
    {
        // Needs to be set
        public string CurrentUserId { get; set; }
        private const string ConnectionString = "Server=.;Database=Registration;Trusted_Connection=True;MultipleActiveResultSets=true";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }

        public RegistrationDbContext()
        {
            Database.Migrate();
        }

        public RegistrationDbContext(DbContextOptions<RegistrationDbContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<PersonEntity> Person { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserEntity>().ToTable("User");
            modelBuilder.Entity<RoleEntity>().ToTable("Role");
            modelBuilder.Entity<UserClaimEntity>().ToTable("UserClaim");
            modelBuilder.Entity<RoleClaimEntity>().ToTable("RoleClaim");
            modelBuilder.Entity<UserRoleEntity>().ToTable("UserRole");
            modelBuilder.Entity<UserLoginEntity>().ToTable("UserLogin");
            modelBuilder.Entity<UserTokenEntity>().ToTable("UserToken");

            //using (var context = new DataSeedingContext())
            //{
            //    context.Database.EnsureCreated();

            //    var testBlog = context.Blogs.FirstOrDefault(b => b.Url == "http://test.com");
            //    if (testBlog == null)
            //    {
            //        context.Blogs.Add(new Blog { Url = "http://test.com" });
            //    }
            //    context.SaveChanges();
            //}

            //modelBuilder.Entity<PersonEntity>().HasData(new PersonEntity
            //{
            //    Id = 1,
            //    FirstName = "John",
            //    LastName = "Baron",
            //    DateOfBirth = DateTime.Now.AddYears(-30),
            //    AdditionalInfo = "This is user for Administration"
            //});

            //modelBuilder.Entity<UserEntity>().HasData(new UserEntity
            //{
            //    Id = 1,
            //    PersonId = 1,
            //    UserName = "admin@admin.com",
            //    NormalizedUserName = "ADMIN@ADMIN.COM",
            //    Email = "admin@admin.com",
            //    NormalizedEmail = "ADMIN@ADMIN.COM",
            //    EmailConfirmed = false,
            //    PasswordHash = "AQAAAAEAACcQAAAAEJUgU8fiOdvmPE3SXEEKuOsRMiZzobbjEGauiv/dVQagKMabhpzD2ZP1O7qPh/hxpg==",
            //    SecurityStamp = "G3JVJUMEE64Y6YG2NCW72LJDUNLK5DVE",
            //    ConcurrencyStamp = "db3120a1-cce9-45e0-bd10-827b06f55cdb",
            //    PhoneNumber = null,
            //    PhoneNumberConfirmed = false,
            //    TwoFactorEnabled = false,
            //    LockoutEnd = null,
            //    LockoutEnabled = false,
            //    AccessFailedCount = 0,
            //    IsValid = true
            //});
        }

        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditable && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IAuditable)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedAt = now;
                    entity.CreatedBy = CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedAt).IsModified = false;
                }

                entity.UpdatedAt = now;
                entity.UpdatedBy = CurrentUserId;
            }
        }
    }
}
