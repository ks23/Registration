﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DAL.ContextBuilders.Base
{
    public class DbContextFactory<TContext> : IDesignTimeDbContextFactory<TContext>
        where TContext : DbContext, new()
    {
        public TContext CreateDbContext(string[] args)
        {
            //  var builder = new DbContextOptionsBuilder<TContext>();
            //  return new TContext(builder.Options);
            return new TContext();
        }
    }
}
